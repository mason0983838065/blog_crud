<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Commentcontroller;
use App\Http\Controllers\TagController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });
Route::get('/', [AuthController::class, 'login'])->name('user.login');
Route::post('/login', [AuthController::class, 'loginpost'])->name('user.loginpost');
Route::get('/registration', [AuthController::class, 'registration'])->name('user.registrate');
Route::post('/registration', [AuthController::class, 'registrationpost'])->name('user.registrationpost');
Route::get('/logout', [AuthController::class,'logout'])->name('user.logout');
Route::resource('post', PostController::class);

Route::group(['middleware' =>'auth'],function() {
    Route::resource('post', PostController::class);
    Route::get('/posts/filter',[PostController::class, 'filter'])->name('post.filter');
    Route::apiResource('comment', Commentcontroller::class);
    Route::get('/tags/{tag}', [TagController::class,'show'])->name('tag.show');
});



