<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Factories\UserFactory;
use Database\Factories\CommentFactory;
use Database\Factories\PostFactory;
use App\Models\User;
use App\Models\Post;
use App\Models\Comment;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        User::factory(5)->create()->each(function ($user) {
            $posts = Post::factory(3)->create(['user_id' => $user->id]);
            $posts->each(function ($post) {
                Comment::factory(2)->create(['post_id' => $post->id, 'user_id' => $post->user_id]);
            });
        });
    }
}
