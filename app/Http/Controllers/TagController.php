<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Tag;

class TagController extends Controller
{
    //
    public function show(Tag $tag)
    {
        $posts = $tag->posts;

        return view('Tags_Show', compact('tag', 'posts'));
    }
}
