<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Models\User;

class AuthController extends Controller
{
    //
    public function login(){
        if(Auth::check()){
            return redirect(route('post.index'));
        }
        return view('Auth.Authlogin');
    }

    public function loginpost(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended(route('post.index'));
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ])->onlyInput('email');
    }

    public function registration()
    {
        if(Auth::check()){
            return redirect(route('post.index'));
        }
        return view('Auth.AuthRegistrate');
    }

    public function registrationpost(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'password'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect(route('user.registrate'))
                        ->withErrors($validator)
                        ->withInput();
        }

        $data['name']=$request->name;
        $data['email']=$request->email;
        $data['password']=Hash::make($request->password);
        $user=User::create($data);
        return redirect(route('user.login'));
    }
    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect(route('user.login'));
    }

}
