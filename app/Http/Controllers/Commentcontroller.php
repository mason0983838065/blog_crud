<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Http\Requests\CommentFormRequest;

class Commentcontroller extends Controller
{
    //
    public function index()
    {
        $comments = Comment::all();
        return response()->json($comments, 200) ;
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'comment' =>'required',
            'post_id' =>'required',
        ]);
        $comment =new Comment();
        $comment->comment = $data['comment'];
        $comment->user_id=Auth::id();
        $comment->post_id = $data['post_id'];
        $comment->save();

        return response()->json($comment, 201);
    }

    public function show(string $id)
    {
        // $post = Post::find($id);
        // $comments = $post->comments()->orderBy('created_at', 'desc')->get();
        // $post = Post::with('user')->find($id);
        $comments = Comment::with('user')->where('post_id', $id)->get();


        return response()->json($comments);

    }


    public function update(CommentFormRequest $request, string $id)
    {
        // $data = $request->validate([
        //     'comment' => 'required',
        // ]);
        $validated = $request->validated();
        $comment=Comment::find($id);
        $comment->comment = $validated['comment'];
        $comment->save();

        return response()->json($comment);
    }


    public function destroy(string $id)
    {
        $comment = Comment::find($id);
        if (!$comment) {
            return response()->json(['comment' => 'comment not found'], 404);
        }

        $comment->delete();

        return response()->json(['comment' => 'comment deleted'], 204);
    }
}
