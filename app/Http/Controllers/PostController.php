<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\Tag;
use App\Http\Requests\PostFormRequest;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $posts=Post::all();
        return view('home',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //

        return view('postcreate');

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PostFormRequest $request)
    {
        //
        $validated = $request->validated();



        $post = new Post();
        if($request->hasFile("cover")){
            $file=$request->file("cover");
            $imageName=time().'_'.$file->getClientOriginalName();
            $file->move(public_path("cover/"), $imageName);
            $post->image =$imageName;
        }
        else{
            $post->image ='';
        }


        $post->title = $validated['title'];
        // $post->author = $request->input('Author');
        $post->content = $validated['content'];
        $post->user_id = Auth::id();
        $post->save();

        $tagNames = explode(',', $request->input('tags'));
        $tags = [];
        foreach ($tagNames as $tagName) {
            $tag = Tag::firstOrCreate(['name' => trim($tagName)]);
            $tags[] = $tag->id;
        }
        $post->tags()->sync($tags);

        return redirect()->route('post.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $post=Post::find($id);
        if (!$post) {
            return redirect()->route('post.index')->with('error', '文章未找到');
        }
        $post->views+=1;
        $post->save();
        return view('post_show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //

        $post=Post::find($id);
        $user=Auth::user();
        return view('post_edit',compact('post','user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PostFormRequest $request, string $id)
    {

        $validated = $request->validated();
        $post=Post::find($id);

        if($request->hasFile("cover")){
            if(File::exists("cover/".$post->image)){
                File::delete("cover/" . $post->image);
            }
            $file=$request->file("cover");
            $imageName=time().'_'.$file->getClientOriginalName();
            $file->move(public_path("cover/"), $imageName);
            $post->image =$imageName;
        }

        $post->title =  $validated['title'];
        $post->content= $validated['content'];;
        $post->created_at =now();
        $post->save();

        $tagNames = explode(',', $request->input('tags'));
        $tags = [];
        foreach ($tagNames as $tagName) {
            $tag = Tag::firstOrCreate(['name' => trim($tagName)]);
            $tags[] = $tag->id;
        }
        $post->tags()->sync($tags);

        return redirect(route('post.show', compact('post')))->with("success","edit video success");

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $post=Post::find($id);
        $post->tags()->detach();
        $post->delete();
        return redirect(route('post.index'));
    }

    public function filter(Request $request)
    {
        $search=$request->search;

        $posts = Post::whereHas('user', function ($query) use ($search) {
            $query->where('name', 'like', '%' . $search . '%');
        })
        ->orWhere('title', 'like', '%' . $search . '%')
        ->get();
        if ($posts->isEmpty()) {
            $posts = [];
            return view('home')->with(['error' => '文章未找到', 'posts' => $posts, 'search' => $search]);
        }
        return view('home',compact('posts','search'));
    }
}
