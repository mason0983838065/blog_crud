<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="container">

        <div class="mt-5">
          @if ($errors->any())
            <div class="col-12">
              @foreach ($errors->all() as $error)
                <div class="alert alert-danger">{{$error}}</div>
              @endforeach
            </div>
          @endif

          @if (session()->has('error'))
            <div class="alert alert-danger">{{session('error')}}</div>
          @endif

          @if (session()->has('success'))
            <div class="alert alert-success">{{session('success')}}</div>
          @endif
        </div>

        <form action="{{route('user.loginpost')}}" method="POST" class="ms-auto me-auto mt-auto" style="500px">
          @csrf
          <div class="mb-3">
              <label  class="form-label">Email address</label>
              <input type="email" class="form-control" name="email">
            </div>
          <div class="mb-3">
            <label  class="form-label">Password</label>
            <input type="password" class="form-control" name="password">
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <div class="" role="alert">
             未註冊請點選<a href="{{route('user.registrate')}}" class="alert-link">此處</a>進行註冊
        </div>
      </div>
</body>
</html>
