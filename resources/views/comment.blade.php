<div >
    <h2>Comment</h2>
    <div id="app">
        <div v-if="errorMessage" class="alert alert-danger">@{{ errorMessage }}</div>

        <div class="form-group">
            <form id="commentForm" @submit.prevent="submitComment">
              @csrf
              <div class="input-group">
                <input type="text" v-model="newComment" class="form-control" name="comment" id="" aria-describedby="helpId" placeholder="enter your comment">
                <input type="hidden" name="post_id"  v-model="post_id"  class="form-control" value="{{$post_id}}">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <div id="comments" >
            <div v-for="comment in comments" :key="comment.id" class="list-group">
                <div  class="list-group-item">
                    <div class="d-flex w-100 justify-content-between">
                        <h6 class="mb-1">@{{comment.user.name}}</h6>
                        <small>@{{comment.created_at | formatDate}}</small>
                    </div>
                    @{{ comment.comment }}
                    <div class="d-flex justify-content-end">
                        <button @click="editComment(comment)" v-if="isCommentAuthor(comment)" class="btn btn-outline-primary " style="margin-left: 10px;">Edit</button>
                        <button @click="deleteComment(comment.id)"  v-if="isCommentAuthor(comment)" class="btn btn-outline-danger  " style="margin-left: 10px;" onclick="return confirm('確定要刪除嗎');">Delete</button>
                    </div>
                </div>
            </div>
            <form v-if="editingComment" @submit.prevent="updateComment">
                <div class="input-group">
                    <input type="text" v-model="editedComment" class="form-control" placeholder="edit your comment">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
          </div>

    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-router/3.0.1/vue-router.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vuex/3.0.1/vuex.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
<script>
    new Vue({
        el:'#app',
        data:{
            comments:[],
            newComment:'',
            post_id:'',
            editingComment: false,
            editedComment: '',
            commentToEdit: '',
            errorMessage:'',
        },
        filters:{
            formatDate: function(value){
                const date = new Date(value);
                return date.toLocaleString();
            }
        },
        methods: {
            submitComment:function(){
                axios.post('/comment',{comment: this.newComment, post_id: {{$post_id}}})
                    .then(response =>{
                        console.log(response.data);
                        this.newComment='';
                        this.refreshComments();
                    })
                    .catch(error =>{
                        console.error("ERRRR:: ",error.response.data);
                        this.errorMessage = error.response.data.message;
                    });
            },
            refreshComments:function(){
                axios.get('/comment/{{$post_id}}')
                    .then(response =>{
                        console.log('Response data:', response.data);
                        if(response.data && response.data.length > 0){
                            this.comments=response.data;
                            this.errorMessage='';
                        }
                        else{
                            this.comments = [];
                        }

                    })
                    .catch(error =>{
                        console.error(error.response.data);
                    });
            },
            editComment:function(comment){
                this.editingComment = true;
                this.editedComment = comment.comment;
                this.commentToEdit = comment.id;

            },
            updateComment:function() {
                console.log('Editing comment with comment:', this.editedComment);
                axios.put('/comment/'+this.commentToEdit,{comment: this.editedComment})
                .then(response =>{
                    console.log('Response data:', response.data);
                    this.editedComment = '';
                    this.editingComment = false;
                    this.refreshComments();
                })
                .catch(error =>{
                    console.error(error.response.data);
                    this.errorMessage = error.response.data.message;
                });
            },
            deleteComment:function(commentId){
                console.log(commentId);
                axios.delete('/comment/'+commentId)
                    .then(response => {
                        console.log(response.data);
                        this.refreshComments();
                    })
                    .catch(error =>{
                        console.error(error);
                    })
            },
            isCommentAuthor(comment) {
                const currentUser = {!! json_encode(Auth::user()) !!};
                if (currentUser && comment.user) {
                    return currentUser.name === comment.user.name;
                }
                return false;
            },
        },
        mounted: function() {
            //
            this.refreshComments();
        },
    });

</script>
