<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <title>Document</title>
</head>
<body>
    <div class="container-fluid">
        <div class="card-group">
            <div class="card">
                <div class="card-header">
                    <h1>Tag: {{ $tag->name }}</h1>
                </div>
                <div class="card-body">
                    <h2>Posts with this tag:</h2>
                    @if ($posts->count() > 0)
                        <ul class="list-group">
                            @foreach ($posts as $post)
                                <li class="list-group-item"><a href="{{ route('post.show', $post->id) }}">{{ $post->title }}</a></li>
                            @endforeach
                        </ul>
                    @else
                        <p>No posts found with this tag.</p>
                    @endif
                </div>
            </div>
        </div>



    </div>
</body>
</html>


