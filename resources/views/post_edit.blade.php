<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <div class="container-fluid">
        <div class="card-group">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Post</h2>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{route('post.update',$post->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method("put")
                        <input type="text" class="form-control m-2" name="title" id="" placeholder="title" value="{{$post->title}}">
                        <input type="text" class="form-control m-2" name="Author" id="" placeholder="Author" value="{{$user->name}} " readonly="readonly">
                        <input type="text" class="form-control m-2" name="tags" placeholder="Tags" value="@foreach ($post->tags as $tag){{$tag->name}}@if (!$loop->last), @endif @endforeach">
                        <textarea  class="form-control" name="content" id="" cols="30" rows="10" placeholder="content" >{{$post->content}}</textarea>
                        <img src="/cover/{{$post->image}}"  class="img-responsive" style="max-height:100px; max-width:100px" alt="" sizes="" srcset=""></td>
                        <input class="form-control m-2" name="cover" type="file" id="" >
                        <button type="submit" class="btn btn-success mt-3">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
