<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <div class="container-fluid">
        <div class="card-group">
            <div class="card">
                <div class="card-header">
                    <div class="row py-2">
                        <div class="col-md-9">
                            <h2 class="">Title: {{ $post->title }}</h2>
                        </div>
                        <div class=" d-flex justify-content-end align-items-center">
                            <a href="{{ route('post.index') }}" class="btn btn-primary" style="margin-left: 10px;">Back to Post List</a>
                            @can('delete-post',$post)
                                <form action="{{ route('post.destroy', $post->id) }}" method="POST">
                                    <button class="btn btn-outline-danger "  style="margin-left: 10px;"onclick="return confirm('確定要刪除嗎');" type="submit">Delete</button>
                                    @csrf
                                    @method('delete')
                                </form>
                            @endcan
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <p>Author: {{ $post->user->name }}</p>
                    <p>Views: {{ $post->views }}</p>
                    <p>Release Time: {{ $post->created_at->format('Y-m-d H:i:s') }}</p>
                    @foreach ($post->tags as $tag)
                        <a href="{{ route('tag.show', $tag) }}">{{ $tag->name }}</a>
                    @endforeach

                    <hr>
                    <p>content:</p>
                    @can('update-post',$post)
                        <a name="" id="" class="btn btn-outline-primary" href="{{ route('post.edit', $post->id) }}" role="button">Edit</a></td>
                    @endcan
                    <div>{{ $post->content }}</div>
                    <img src="/cover/{{$post->image}}"  class="img-responsive" style="max-height:100px; max-width:100px" alt="" sizes="" srcset="">

                    <hr>
                    @include('comment',['post_id'=>$post->id])

                </div>
            </div>
    </div>
</body>
</html>
