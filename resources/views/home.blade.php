<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".post_row").click(function () {
                window.location = $(this).data("href");
            });
        });
    </script>
    <title>Document</title>
</head>
<body>
    <div class="container-fluid">
        <div class="card-group">
            <div class="card">
                <div class="card-header row">
                    <div class="col-md-6">
                        <h1>Blog_CRUD </h1>
                    </div>
                    <div class="col-md-6 d-flex justify-content-end align-items-center">
                        <h2>Welcome {{Auth::user()->name}}</h2>
                        <a name="" id="" class="btn btn-primary" href="{{route('user.logout')}}" role="button">Logout</a>
                        @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="row py-2">
                        <div class="col-md-3">
                            <a name="CreatePost" id="" class="btn btn-success" href="{{route('post.create')}}" role="button">Add New Post</a>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <form action="{{ route('post.filter') }}"method="get">
                                    <div class="input-group">
                                        <input type="text" name="search" id="" class="form-control" placeholder="Search..." value="{{isset($search) ? $search : ''}}" >
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Author</th>
                                <th scope="col">Views</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                            <tr class="post_row" data-href="{{ route('post.show', $post->id) }}">
                                <th scope="row">{{$post->id}}</th>
                                <td>{{$post->title}}</td>
                                <td>{{$post->user->name}}</td>
                                <td>{{$post->views}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
